<?php /* Template Name: Post Index*/
query_posts('post_type=post');
?>
<?php get_header();

include "iframe-header.php";
?>
	<div class = "article-container">
	<?php

		if (have_posts()) :

			$count = 0;

			while (have_posts()) :  the_post();
				
				include 'post-header.php';
			
			endwhile;
		endif;

	?>

	</div><!-- article-container -->


<?php get_footer();?>