<div class ="post-header">
    <div class ="title-author-container">
        <h2><a href =<?php echo get_permalink(); ?>><?php the_title(); ?></a></h2>
        
        <p class = "post-header-category"><?php
            foreach((get_the_category()) as $category) { 
                echo $category->cat_name . ' '; 
            } 
        ?></p>      
        
        <p class ="author-meta-container"> 
            <a target ="_blank" href =<?php echo get_the_author_meta('user_url'); ?>>
                Written By <?php echo get_the_author_meta('display_name'); ?>
            </a>
            <br>
            <?php echo get_the_date(); ?>
        </p>
        
    </div> 

        <!-- Song changer Audio player button. Only display & run script if there is a song associated with the post -->

        <?php 
            if (get_field('music')) :
        ?>   

            <div class = "change-song category-btn">
                <img class = "vinyl-icon" src ="/wp-content/themes/bluesmaps/icon/vinyl.svg">
            </div>

            <script>

                document.querySelectorAll('.vinyl-icon')[<?php echo $count++ ?>].onclick = function(){

                    // run a check to see if the song in the audio player is the same as this one. If so dont reload all this

                    if (audio.src != '<?php the_field("music"); ?>'){

                        audio.src = '<?php the_field("music"); ?>';
                        parent.document.getElementById('artistName').textContent = '<?php the_field("artist_name"); ?>';
                        parent.document.querySelector('#songName').textContent = "<?php the_field('song_title'); ?>";
                        parent.document.getElementById('yearRecorded').textContent = '<?php the_field("song_year_recorded"); ?>';

                        this.classList.add('currentSong');
                    }
                    
                    musicIconToggler();
                    this.classList.toggle('spinning');
                    // When clicking on a vinyl icon search if there is another icon spinning and remove it
                    jQuery('.spinning').not(this).removeClass('spinning');
                    jQuery('.currentSong').not(this).removeClass('currentSong');
                    playPause(audio);
                }

            </script>

        <?php  
            endif; 
        ?>

    <a class ="marker-image-container" href =<?php echo get_permalink(); ?>><figure class ="marker-image-index" style = "background-image:url(<?php echo get_field('img') ?>)">     
        
    </figure></a>
</div>

