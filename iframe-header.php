<header id ="site-header">
    <h1>
        <a class = 'site-title' href=<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>>
            <?php echo get_bloginfo('name'); ?>
        </a>
    </h1>

    <img class ='logo-icon' src ="/wp-content/themes/bluesmaps/icon/road.svg">
</header>