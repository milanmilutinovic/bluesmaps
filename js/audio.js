const audio = parent.document.getElementById('audio');
const audioBtn = parent.document.getElementById('audio-btn');
const musicIcon = parent.document.getElementById('gramophone-music-icon');

function playPause(song){
   if (song.paused && song.currentTime >= 0 && !song.ended) {
      song.play();
      jQuery('.currentSong').addClass('spinning');
   } else {
      song.pause();
      jQuery('.spinning').removeClass('spinning');
   }
}

function reset(btn, song){
   if(btn.classList.contains('playing')){
      btn.classList.toggle('playing');
   }
   song.pause();
   musicIcon.classList.remove('music-on');
   song.currentTime = 0;
}

function progress(btn, song){
   setTimeout(function(){
      var end = song.duration; 
      var current = song.currentTime;
      var percent = current/(end/100);
      //check if song is at the end
      if(current==end){
         reset(btn, song);
      }
      //set inset box shadow
      btn.style.boxShadow = "inset " + btn.offsetWidth * (percent/100) + "px 0px 0px 0px rgba(0,0,0,0.125)"
      //call function again
      progress(btn, song);     
   }, 133.7);
}

function musicIconToggler() {
   if (audio.paused){
      musicIcon.classList.add('music-on');
   }
   
   if (!audio.paused){
      musicIcon.classList.remove('music-on');
   }
}

audioBtn.addEventListener('click', function(){
   musicIconToggler();
   audioBtn.classList.toggle('playing');
   playPause(audio);
   progress(audioBtn, audio);
});