<?php
while ( have_posts() ) : the_post();

	$heading[] = get_the_title();

	/// str_replace is used to sanitize fields of single apostrophes, otherwise they will break the website //

	$description[] = str_replace("'", '&#8217;', get_field('description'));
	$img[] = get_field('img');
	$location[] = str_replace("'", '&#8217;', get_field('location'));
	$lat[] = get_field('lat');
	$lng[] = get_field('lng');
	$yearof[] = get_field('year');
	$permalinks[] = get_permalink();
    $categoryString[] = get_field('category');
    
endwhile; ?>



<script>
mapboxgl.accessToken = 'pk.eyJ1Ijoic29uaG91c2UiLCJhIjoiY2p5cTMwNnZ2MW01bDNtbG16d3U5eHR4YSJ9.vq08VmKYM0DD7bM5aIXIeQ';
	
var map = new mapboxgl.Map({
    container: document.querySelector("#map"), 
    style: 'mapbox://styles/sonhouse/cjyq430et3m2p1cpmuct0nc8p?optimize=true',
    center: [-89.716, 32.408], 
    zoom: 6.25, 
    maxZoom: 10.25,
    minZoom: 6,
    keyboard: false,
    maxBounds: [-93, 29.5, -86.5, 36.6],
    // trackResize: false,
    // pitch: 10,
    renderWorldCopies: false, // Helps Performance? Research
    // crossSourceCollisions: false
    dragRotate: false
});

// convert from php arrays to JS arrays. Must be double parsed

let acf = {
    title: 			JSON.parse('<?php echo json_encode($heading); ?>'),
    description: 	JSON.parse('<?php echo json_encode($description); ?>'),
    location: 		JSON.parse('<?php echo json_encode($location); ?>'),
    year: 			JSON.parse('<?php echo json_encode($yearof); ?>'),
    link: 			JSON.parse('<?php echo json_encode($permalinks); ?>'),
    lat:			JSON.parse('<?php echo json_encode($lat); ?>'),
    lng:			JSON.parse('<?php echo json_encode($lng); ?>'),
    img:			JSON.parse('<?php echo json_encode($img); ?>'),
    category:		JSON.parse('<?php echo json_encode($categoryString); ?>'),
}


var nav = new mapboxgl.NavigationControl({
    // visualizePitch: true
    showCompass: false
});
    map.addControl(nav, 'bottom-right');
    map.addControl(new mapboxgl.FullscreenControl({container: document.querySelector('body')}));

var size = 80;

var pulsingDot = {
    width: size,
    height: size,
    data: new Uint8Array(size * size * 4),
    
    onAdd: function() {
        var canvas = document.createElement('canvas');
        canvas.width = this.width;
        canvas.height = this.height;
        this.context = canvas.getContext('2d');
    },
    
    render: function() {
        var duration = 1000;
        var t = (performance.now() % duration) / duration;
        
        var radius = size / 2 * 0.3;
        var outerRadius = size / 2 * 0.7 * t + radius;
        var context = this.context;
    
        // draw outer circle
        context.clearRect(0, 0, this.width, this.height);
        context.beginPath();
        context.arc(this.width / 2, this.height / 2, outerRadius, 0, Math.PI * 2);
        context.fillStyle = 'rgba(145, 179, 212,' + (1 - t) + ')';
        context.fill();
        
        // draw inner circle
        context.beginPath();
        context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
        context.fillStyle = 'rgba(145, 179, 212, 1)';
        context.strokeStyle = 'white';
        context.lineWidth = 2 + 4 * (1 - t);
        context.fill();
        context.stroke();
        
        // update this image's data with data from the canvas
        this.data = context.getImageData(0, 0, this.width, this.height).data;
        
        // keep the map repainting
        map.triggerRepaint();
        
        // return `true` to let the map know that the image was updated
        return true;
    }
};

map.on('load', function () {

    map.addImage('pulsing-dot', pulsingDot, { pixelRatio: 3 });
 
    markers = [];   

    for (let i = 0; i < acf.location.length; i++){

    let markerIcon = document.createElement('div');

        // Change Marker Icons based on Category ID

        if ( acf.category[i] == 3){
            // Bluesmen
            markerIcon.innerHTML = ('<img class ="cat-3-marker" src = "<?php echo get_field( 'marker_icon', 'category_3' );?>">');
        }

        if ( acf.category[i] == 5){
            // Record Shops & Studios
            markerIcon.innerHTML = ('<img class ="cat-5-marker" src = "<?php echo get_field( 'marker_icon', 'category_5' );?>">');
        }

        if ( acf.category[i] == 6){
            // Landmarks
            markerIcon.innerHTML = ('<img class ="cat-6-marker" src = "<?php echo get_field( 'marker_icon', 'category_6' );?>">');
        }
         
        if ( acf.category[i] == 7){
            // Graveyards-Churches
            markerIcon.innerHTML = ('<img class ="cat-7-marker" src = "<?php echo get_field( 'marker_icon', 'category_7' );?>">');
        }

        if ( acf.category[i] == 8){
            // Bars, Jukes & Clubs
            markerIcon.innerHTML = ('<img class ="cat-8-marker" src = "<?php echo get_field( 'marker_icon', 'category_8' );?>">');
        }

    let lngLat = [parseFloat(acf.lng[i])*-1, parseFloat(acf.lat[i])];

    let marker = new mapboxgl.Marker({
        // color: 'red',
        element: markerIcon,
        anchor: "bottom"
    })
        .setLngLat(lngLat)
            .setPopup(
                new mapboxgl.Popup({
                    offset: -1,
                    // anchor: 'left'
                })
                    .setHTML('<figure><a target ="search_iframe" href =' + acf.link[i] + '><img class = "marker-image" src =' + acf.img[i] + '></a>'+
                    '<figcaption>' + acf.location[i] + '<strong> (' + acf.year[i] + ')</strong></figcaption></figure>'+
                    '<h3>' + acf.title[i] + '</h3>'+
                    '<p class = "marker-desc">' + acf.description[i] + '</p>'+
                    '<a class = "learn-more-button" target ="search_iframe" href =' + acf.link[i] + '><span class ="marker-button-text">Learn More</span> <span class="dashicons dashicons-share-alt2"></span></a>'))
            .addTo(map);

            map.addLayer({
            "id": "glow"+i,
            "category" : acf.category[i].cat_ID,
            "type": "symbol",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                            "geometry": {
                            "type": "Point",
                            "coordinates": lngLat
                            }

                    }]
                }
            },
            "layout": {
                "icon-image": "pulsing-dot"
            }
        });

        markers.push(marker);

    }

});

let k = [3,5,6,7,8];

for (let i = 0; i < k.length; i++){

    jQuery(".cat-"+k[i]+"-btn").click(function(){
        jQuery(".cat-"+k[i]+"-marker").toggle();
    });

}

</script>