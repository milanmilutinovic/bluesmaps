<!DOCTYPE html>
<html id = "html">

	<head>

		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
		<meta name="description" content="Deep Blues Trails is an interactive history map based website focused on telling the origin story of the Delta Blues, in Mississippi." />
		<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>

		<meta property="og:title" content="<?php bloginfo('name'); ?><?php wp_title(); ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:description" content="Deep Blues Trails is an interactive history map based website focused on telling the origin story of the Delta Blues, in Mississippi." />
		<meta property="og:url" content="https://bluesmaps.info" />
		<meta property="og:image" content="https://bluesmaps.info/wp-content/themes/bluesmaps/icon/charley2.jpg" />

		<meta property="fb:app_id" content="2259162661062961">

		<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
		<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet' />

		<?php wp_head();?>

	</head>

<body <?php body_class();?>>

	<div id="mobile-message">
		<h1><?php echo get_bloginfo('name'); ?></h1>
		<img class ='logo-icon' src ="/wp-content/themes/bluesmaps/icon/road.svg">
		<p><?php bloginfo('name'); ?> is not currently available at this screen size. Try changing to Landscape to View, or a bigger device</p>
	</div>

	<div class ='desktop-container'>
