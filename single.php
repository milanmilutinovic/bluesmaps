<?php

    get_header(); 
    
    include "iframe-header.php";

        $count = 0;

        // Start the loop.
        while ( have_posts() ) : the_post();

            include 'post-header.php';
       
       ?>
            <div class ="post-content">
            <?php 
            
                the_content();
                // get_template_part( 'template-parts/content/content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }
            ?>

            </div><!-- .post-content -->
        </article><!-- .site-main -->
            
            
            <?php the_post_navigation( array(

                'prev_text' => 
                '<div class="dashicons dashicons-arrow-left-alt"></div><p>%title</p>',
                
                'next_text' => 
                '<div class="dashicons dashicons-arrow-right-alt"></div><p>%title</p>',
                
            ) );
 
        // End the loop.
        endwhile;

        ?>
        
<?php get_footer(); ?>