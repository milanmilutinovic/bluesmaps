<?php /* Template Name: Map Page & Index */
query_posts('post_type=post'); 

get_header();?>




<div class="flex-container">
	<div id="map">

		<nav class="map-nav">
			<div id ="audio-wrapper">
				<?php $music = get_field('music', 'option'); ?>
				<audio id="audio" src="<?php echo $music['url']; ?>" preload="metadata" type="audio/mpeg">
					Your browser does not support the audio element.
				</audio>
				
				<a class = "gramophone btn" id="audio-btn">
					<img class = "btn-icon" src ="/wp-content/themes/bluesmaps/icon/gramophone.png">
					<img id ="gramophone-music-icon" src ="/wp-content/themes/bluesmaps/icon/music.svg">
				</a>

				
				<?php 
					$artistName = get_field('artist_name', 'option'); 
					$songName = get_field('song_title', 'option');
					$yearRecorded = get_field('song_year_recorded', 'option');
				?>


				<div class="tooltip">
					<h3 id ="artistName">
						<?php echo $artistName; ?>
					</h3>
					<span>
						<?php 
							echo '<span id ="songName">'; echo $songName; echo '</span>'; 
							echo ' ('; echo '<span id ="yearRecorded">'; echo $yearRecorded; echo '</span>'; echo ')'; 
						?>
					</span>
				</div>


			</div>

			<div class ="toggle-marker-categories">

				<div class = "cat-7-btn category-btn">
					<img class = "btn-icon" src ="<?php echo get_field( 'marker_button', 'category_7' );?>">
				</div>

				<div class = "cat-8-btn wide-btn category-btn">
					<img class = "btn-icon" src ="<?php echo get_field( 'marker_button', 'category_8' );?>">
				</div>
				
				<div class = "cat-3-btn category-btn">
					<img class = "btn-icon" src ="<?php echo get_field( 'marker_button', 'category_3' );?>">
				</div>		
		
				<div class = "cat-5-btn category-btn">
					<img class = "btn-icon" src ="<?php echo get_field( 'marker_button', 'category_5' );?>">
				</div>

				<div class = "cat-6-btn category-btn">
					<img class = "btn-icon" src ="<?php echo get_field( 'marker_button', 'category_6' );?>">
				</div>			

			</div>

		</nav>


	</div>


	<iframe id="article-iframe" name ="search_iframe" src="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" frameborder=0></></iframe>

</div>

<!-- desktop-container --></div>

</body>

<?php include 'js/map.php' ?>

</html>
