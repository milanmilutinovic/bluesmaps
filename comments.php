<?php 
if ( post_password_required() )
    return;
?>
 
<div id="comments" class="comments-area">
 
    <?php if ( have_comments() ) : ?>
        <h2 class="comments-title">
            <?php
                printf('Some thoughts on %s', get_the_title() );
            ?>
        </h2>
 
        <ol class="comment-list">
            <!-- This HTML is in functions.php under format_comment-->
            <?php wp_list_comments('type=comment&callback=format_comment'); ?>
        </ol><!-- .comment-list -->
 
    <?php endif; // have_comments() ?>
 
    <?php comment_form(); ?>
 
</div><!-- #comments -->