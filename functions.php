<?php

require get_template_directory() . '/inc/functions-remove_admin_defaults.php';
require get_template_directory() . '/inc/functions-custom_admin.php';

function load_stylesheets(){
    // Good pre-built animations, but don't load them. Just use this as a test
    // wp_enqueue_style( 'animate', get_template_directory_uri() . '/animate/animate.css' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_script( 'audio', get_template_directory_uri() . '/js/audio.js' );
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'load_stylesheets');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

// Allow SVG to media uploader
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');

// Allow console logging in php
function console_log( $data ){
	echo '<script>';
	echo 'console.log('. json_encode( $data ) .')';
	echo '</script>';
}

function replace_admin_menu_icons_css() {
    ?>
    <style>
        .dashicons-admin-post:before {
            content: '\f231';
        }
        .dashicons-admin-media:before{
            content: '\f128';
        }
    </style>
    <?php

}
//
// ACF LOCAL JSON 
//
function site_acf_json_save_point($path) {
    $path = get_stylesheet_directory() . '/acf/';
    return $path;
}
add_filter('acf/settings/save_json','site_acf_json_save_point');

function site_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);

    // append path
    $paths[] = get_stylesheet_directory() . '/acf/';

    // return
    return $paths;

}
add_filter('acf/settings/load_json', 'site_json_load_point');

add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );

function load_dashicons_front_end() {
  wp_enqueue_style( 'dashicons' );
}

add_action( 'admin_head', 'replace_admin_menu_icons_css' );

// Make the login look great
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/icon/charley2.jpg);
            border-radius:999px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function disable_wp_emojicons() {

    // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  
    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
    add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyCHiSORpvyauDMaBWsBvJjmqsgBK3kKL1c');
}

add_action('acf/init', 'my_acf_init');

/// Custom COMMENT HTML
function format_comment($comment, $args, $depth) {

    $GLOBALS['comment'] = $comment; ?>
    
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            
        <div class="comment-intro">
            <?php echo get_avatar( $comment, 50 ); ?>
            
            <?php echo '<span>'; printf(__('%s'), get_comment_author_link()); echo '</span>'; ?>
            <a class="comment-permalink" href="<?php echo htmlspecialchars ( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s'), get_comment_date(), get_comment_time()) ?></a>
            
        </div>
        
        <?php if ($comment->comment_approved == '0') : ?>
            <em><php _e('Your comment is awaiting moderation.') ?></em><br />
        <?php endif; ?>
        
        <?php comment_text(); ?>
        
        <div class="reply">
            <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </div>
    
<?php } ?>